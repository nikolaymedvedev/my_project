FROM python

COPY * .

WORKDIR .

RUN pip install pytest
RUN pip install requests
RUN pip install allure-pytest

CMD pytest test_for_check_v_1.py