import requests
from logger import my_log


class MyRequests:

    def get_method(self, url: str):
        return self.api_requests("GET", url)

    def post_method(self, url: str):
        return self.api_requests("POST", url)

    def api_requests(self, method, url):
        try:
            my_log().info(f"{method} {url}")
            return requests.request(method, url)
        except Exception as error:
            my_log().error(f"ОШИБКА: {error}")
        return None
