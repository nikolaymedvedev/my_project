import allure

from api_requests import MyRequests


class ApiMethods:

    @allure.step("Отправка запроса")
    def get_information(self, url: str):
        response = MyRequests().get_method(url=url)
        assert response.status_code == 200
        return response

    @allure.step("Проверка запроса")
    def check_info(self, inform):
        assert len(inform.content) > 0
